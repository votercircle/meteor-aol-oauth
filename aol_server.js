AOL = {};

OAuth.registerService('aol', 2, null, function(query) {
  var tokens = getTokens(query);
  var accessToken = tokens.access_token;
  var identity = getIdentity(accessToken);
  var userData= identity.response.data.userData;
  var expiresAt = (new Date).getTime()/1000 + tokens.expires_in;

  return {
    serviceData: {
      id: userData.loginId,
      accessToken: OAuth.sealSecret(accessToken),
      email: userData.loginId+"@aol.com",
      username: userData.loginId,
      expiresAt:expiresAt
    },
    options: {profile: {name: userData.displayName}}
  };
});

var userAgent = "Meteor";
if (Meteor.release)
  userAgent += "/" + Meteor.release;

var getTokens = function (query) {
  var config = ServiceConfiguration.configurations.findOne({service: 'aol'});
  if (!config)
    throw new ServiceConfiguration.ConfigError();

  var response;
  try {
    response = HTTP.post(
      "https://api.screenname.aol.com/auth/access_token", {
        headers: {
          Accept: 'application/json',
          "User-Agent": userAgent
        },
        params: {
          code: query.code,
          client_id: config.clientId,
          client_secret: OAuth.openSecret(config.secret),
          redirect_uri: OAuth._redirectUri('aol', config),
          grant_type:'authorization_code',
          state: query.state
        }
      });
  } catch (err) {
    throw _.extend(new Error("Failed to complete OAuth handshake with AOL. " + err.message),
                   {response: err.response});
  }
  if (response.data.error) { // if the http response was a json object with an error attribute
    throw new Error("Failed to complete OAuth handshake with AOL. " + response.data.error);
  } else {
    return response.data;
  }
};

var getIdentity = function (accessToken) {
  try {

    var response = HTTP.get(
        "https://api.screenname.aol.com/auth/getUserData", {
        headers: {
            "User-Agent": userAgent
        },
        params: {
            Authorization: "Bearer " + accessToken,
            access_token: accessToken,
            f : "json"
        }
    });
    return response.data;
  } catch (err) {
    throw _.extend(new Error("Failed to fetch identity from aol. " + err.message),
                   {response: err.response});
  }
};


AOL.retrieveCredential = function(credentialToken, credentialSecret) {
  return OAuth.retrieveCredential(credentialToken, credentialSecret);
};
