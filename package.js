Package.describe({
    name: 'votercircle:meteor-aol-oauth',
    summary: "AOL OAuth flow",
    version: "1.0.4",
    git: 'https://votercircle.bitbucket.com/votercircle/meteor-aol-oauth',
    documentation: 'README.md'
});

Package.on_use(function(api) {
  api.versionsFrom("METEOR@0.9.2")
  api.use('oauth2', ['client', 'server']);
  api.use('oauth', ['client', 'server']);
  api.use('http', ['server']);
  api.use('underscore', 'client');
  api.use('templating', 'client');
  api.use('random', 'client');
  api.use('accounts-base', ['client', 'server']);
  api.use('service-configuration', ['client', 'server']);

  api.export('AOL');

  api.add_files('aol_server.js', 'server');
  api.add_files('aol_client.js', 'client');
});
