Template.configureLoginServiceDialogForAOL.siteUrl = function () {
  return Meteor.absoluteUrl();
};

Template.configureLoginServiceDialogForAOL.fields = function () {
  return [
    {property: 'clientId', label: 'Client ID'},
    {property: 'secret', label: 'Client Secret'}
  ];
};
